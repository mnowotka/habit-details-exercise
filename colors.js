export const COLORS = {
  PURPLE_BROWN: 'rgb(35,31,32)',
  FADED_ORANGE: 'rgb(244,119,89)',
  WHITE: 'rgb(255,255,255)',
  GREENISH_TEAL: 'rgb(82,212,154)',
  LIGHT_SAGE: 'rgb(227,248,239)',
  BLACK: 'rgb(0,0,0)',
};
