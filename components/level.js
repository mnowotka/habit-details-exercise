import React from 'react';
import styled from 'styled-components/native';
import * as Progress from 'react-native-progress';
import PropTypes from 'prop-types';
import { COLORS } from '../colors';

const LevelContainer = styled.View`
  flex-direction: row;
  position: absolute;
  right: 0;
`;

const LevelStep = styled.View`
  width: 4;
  border-radius: 5;
  margin-right: 2;
  align-self: flex-end;
  ${props => `height: ${props.index + props.index * 2 + 5}`};
  ${props => (props.isActive ? `background-color: ${COLORS.GREENISH_TEAL}` : `background-color: ${COLORS.LIGHT_SAGE}`)};
`;

export default class Level extends React.Component {
  static propTypes = {
    levels: PropTypes.array.isRequired,
    activeIndex: PropTypes.number.isRequired,
  };

  render() {
    const { levels, activeIndex } = this.props;
    return (
      <LevelContainer>
        {levels.map((level, index) => (
          <LevelStep index={index} isActive={index <= activeIndex} key={index} />
        ))}
      </LevelContainer>
    );
  }
}
