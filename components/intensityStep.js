import React from 'react';
import { Col } from 'native-base';
import styled from 'styled-components/native';
import * as Progress from 'react-native-progress';
import PropTypes from 'prop-types';
import { COLORS } from '../colors';
import Level from './level';

const IntensityStepContainer = styled.View`
  margin-top: 10;
  margin-bottom: 10;
  flex-direction: row;
`;

const Counter = styled.View`
  width: 60;
  height: 60;
  border-radius: 50;
  ${props => (props.isActive ? `background-color: ${COLORS.GREENISH_TEAL}` : `background-color: ${COLORS.LIGHT_SAGE}`)};

  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 16;
`;

const CounterNumber = styled.Text`
  font-size: 18;
  ${props => (props.isActive ? `color: ${COLORS.WHITE}` : `color: ${COLORS.GREENISH_TEAL}`)};
  font-weight: bold;
`;

const LevelContainer = styled.View``;

const LevelInformation = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
`;

const LevelText = styled.Text`
  font-size: 15;
  color: ${COLORS.BLACK};
  margin-top: 10;
  margin-bottom: 10;
  margin-right: 15;
  font-weight: bold;
`;

const LevelCounter = styled(Level)`
  align-self: flex-end;
  margin-left: 10;
  height: 10;
`;

export default class IntensityStep extends React.Component {
  static propTypes = {
    levelDescription: PropTypes.string.isRequired,
    target: PropTypes.string.isRequired,
    levels: PropTypes.array.isRequired,
    levelIndex: PropTypes.number.isRequired,
    time: PropTypes.number.isRequired,
  };

  render() {
    const { levelDescription, time, target, levels, levelIndex } = this.props;
    let previusTarget = 0;

    if (levelIndex > 0) previusTarget = levels[levelIndex - 1].target;

    return (
      <IntensityStepContainer>
        <Col size={20}>
          <Counter isActive={time >= previusTarget}>
            <CounterNumber isActive={time >= previusTarget}>{target}</CounterNumber>
          </Counter>
        </Col>
        <Col size={80}>
          <LevelContainer>
            <LevelInformation>
              <LevelText>{levelDescription.replace('{target}', target)}</LevelText>
              <LevelCounter levels={levels} activeIndex={levelIndex} />
            </LevelInformation>

            <Progress.Bar
              width={null}
              borderRadius={10}
              color={`${COLORS.GREENISH_TEAL}`}
              height={16}
              progress={time >= target ? 100 : (time - previusTarget) / target}
            />
          </LevelContainer>
        </Col>
      </IntensityStepContainer>
    );
  }
}
