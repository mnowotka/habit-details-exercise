import React from 'react';
import { Text, ScrollView } from 'react-native';
import { Icon } from 'native-base';
import styled from 'styled-components/native';
import _ from 'lodash';
import IntensityStep from './components/intensityStep';
import { COLORS } from './colors';

const LeftArrow = styled(Icon)`
  color: ${COLORS.WHITE};
  margin-top: 15;
  font-size: 20;
`;

const ScrollToDown = styled(Icon)`
  color: ${COLORS.WHITE};
  text-align: center;
  margin-top: 15;
  font-size: 40;
`;

const HabitHeaderIcon = styled(Icon)`
  color: ${COLORS.GREENISH_TEAL};
  margin-top: 15;
`;

const HabitHeaderImage = styled.View`
  width: 200;
  height: 200;
  background-color: ${COLORS.WHITE};
  position: absolute;
  top: 190;
  right: 10;
`;

const OpenSource = styled(Icon)`
  color: ${COLORS.FADED_ORANGE};
  font-size: 20;
`;

const HabitHeader = styled.Text`
  color: ${COLORS.BLACK};
  font-weight: bold;
  font-size: 30;
  margin-bottom: 30;
  margin-top: 25;
`;

const HabitDescription = styled.Text`
  color: ${COLORS.BLACK};
  font-size: 23;
`;

const Excercises = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
`;

const HabitBenefits = styled.Text`
  color: ${COLORS.BLACK};
  font-size: 21;
`;

const Instruction = styled.Text`
  color: ${COLORS.BLACK};
  font-size: 21;
`;

const HabitSource = styled.Text`
  color: ${COLORS.FADED_ORANGE};
  font-size: 17;
  margin-bottom: 10;
`;

const HabitCategory = styled.Text`
  color: ${COLORS.WHITE};
  font-size: 17;
`;

const HabitLine = styled.View`
  background-color: ${COLORS.WHITE};
  opacity: 0.4;
  width: 100%;
  height: 1px;
`;

const HabitTitle = styled.Text`
  color: ${COLORS.WHITE};

  font-size: 34;
  margin-top: 4;
  margin-bottom: 20;
`;

const BioMarkers = styled.Text`
  color: ${COLORS.WHITE};
  font-size: 22;
  width: 50%;
  margin-top: 20;
`;

const Header = styled.View`
  background-color: ${COLORS.FADED_ORANGE};
  width: 100%;
  flex: 1;
  padding-top: 50;
  padding: 50px 20px 30px 20px;
`;

const Content = styled.View`
  padding: 20px;
  width: 100%;
`;

const StartHabit = styled.View`
  background-color: ${COLORS.FADED_ORANGE};
  width: 100%;
  border-radius: 15;
  margin-top: 40;
`;

const StartHabitText = styled.Text`
  color: ${COLORS.WHITE};
  padding: 20px;
  font-size: 19;
  text-align: center;
`;

const Excercise = styled.View`
  width: 48%;
  height: 180;
  margin-top: 12;
  background-color: ${COLORS.GREENISH_TEAL};
  border-radius: 12;
  padding: 10px;
`;

const ExcerciseText = styled.Text`
  color: ${COLORS.WHITE};
  font-size: 26;
`;

const ExerciseButton = styled.View`
  border-radius: 50;
  background: ${COLORS.WHITE};
  position: absolute;
  bottom: 20;
  right: 20;
  align-items: center;
  justify-content: center;
  width: 40;
  height: 40;
`;

const ExerciseButtonIcon = styled(Icon)`
  color: ${COLORS.GREENISH_TEAL};
`;

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      habitDetails: {
        contentType: 'habit',
        contentId: 'habit-moderate-exercise',
        type: 'weekly',
        category: 'excercise',
        benefits:
          'Being physically active is a vital part of your overall health. Inactivity puts you at risk for heart disease, obesity and diabetes. You should be exercising regularly to help you improve your cholesterol and triglyceride levels, alleviate stress, drepression and anxiety, improve your sleep and stamina, and to help you manage your body weight. ',
        instructions:
          'How to get started:\n1. Pick one or two of your preferred exercises from the list below, or an alternative exercise of your choice. \n2. Prepare your workout clothing and equipment for your training session.\n3. Schedule a time for your workout session: Set Reminder.\n\nYour habit:\nStart a physical activity that gets your heart pumping. Build up towards your required total time of XX minutes every week, and track your completed training sessions by checking off every successful 15 minute interval.',
        sources:
          '[Physical activity guidelines: How much exercise do you need?](https://www.hsph.harvard.edu/nutritionsource/2013/11/20/physical-activity-guidelines-how-much-exercise-do-you-need/)\n[Effectiveness of altering serum cholesterol levels without drugs](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1312230/)\n[Healthline: How Long Does It Take to Lower Cholesterol?](https://www.healthline.com/health/high-cholesterol/how-long-does-it-take-to-lower)\n[Physical Activity Guidelines for Americans](https://health.gov/paguidelines/guidelines/)\n[Effects of endurance exercise training on plasma HDL cholesterol levels depend on levels of triglycerides: evidence from men of the Health, Risk Factors, Exercise Training and Genetics (HERITAGE) Family Study.](https://www.ncbi.nlm.nih.gov/pubmed/11451756)\n[Physical activity and high density lipoprotein cholesterol levels: what is the relationship?](https://www.ncbi.nlm.nih.gov/pubmed/10593643)\n[Differential Effects of Aerobic Exercise, Resistance Training and Combined Exercise Modalities on Cholesterol and the Lipid Profile: Review, Synthesis and Recommendations](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3906547/)\n[Exercise and circulating cortisol levels: the intensity threshold effect.](https://www.ncbi.nlm.nih.gov/pubmed/18787373)\n[Moderate-to-Vigorous Physical Activity and All-Cause Mortality: Do Bouts Matter?](https://www.ncbi.nlm.nih.gov/pubmed/29567764)\n[Exercise training improves free testosterone in lifelong sedentary aging men](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5510446/)',
        title: 'Moderate intensity training',
        defaultLevelDescription: '{target} minutes every week',
        levels: [{ target: 60 }, { target: 90 }, { target: 120 }, { target: 150 }],
        excercises: [
          { name: 'Gym training', image: '' },
          { name: 'Cycling', image: '' },
          { name: 'Slow jogging', image: '' },
          { name: 'Team sports', image: '' },
        ],
        biomarkers: ['Cholesterol', 'Vitamin D', 'Zinc', 'Magnesium'],
      },
    };
  }

  _getSourceUrl = source => {
    const splittedSource = _.split(source, ']');

    if (splittedSource.length === 2) {
      return splittedSource[1].replace('(', '').replace(')', '');
    }
  };

  _getSourceTitle = source => {
    const splittedSource = _.split(source, ']');

    if (splittedSource.length === 2) {
      return splittedSource[0].replace('[', '');
    }
  };

  render() {
    const { habitDetails } = this.state;
    return (
      <ScrollView>
        <Header>
          <ScrollToDown name="chevron-down" type="Entypo" />
          <HabitCategory>{habitDetails.category}</HabitCategory>
          <HabitTitle>{habitDetails.title}</HabitTitle>
          <HabitLine />
          <BioMarkers>
            <LeftArrow name="arrow-right" type="Feather" />
            {habitDetails.biomarkers.join(', ')}
          </BioMarkers>
          <HabitHeader>Your habit</HabitHeader>
          <HabitDescription>{habitDetails.benefits}</HabitDescription>
          <HabitHeaderImage />
        </Header>
        <Content>
          <HabitHeader>
            Intensity <HabitHeaderIcon name="ios-information-circle-outline" type="Ionicons" />
          </HabitHeader>

          {habitDetails.levels.map((level, index) => (
            <IntensityStep
              time={40}
              levelIndex={index}
              levels={habitDetails.levels}
              levelDescription={habitDetails.defaultLevelDescription}
              target={level.target}
              key={index}
            />
          ))}

          <HabitHeader>How you should start</HabitHeader>
          <Instruction>{habitDetails.instructions}</Instruction>
          <HabitHeader>Your benefits</HabitHeader>
          <HabitBenefits>{habitDetails.benefits}</HabitBenefits>
          <HabitHeader>Exercises</HabitHeader>
          <Excercises>
            {habitDetails.excercises.map((excercise, index) => (
              <Excercise key={index}>
                <ExcerciseText>{excercise.name.replace(' ', '\n')}</ExcerciseText>
                <ExerciseButton>
                  <ExerciseButtonIcon name="dots-three-horizontal" type="Entypo" />
                </ExerciseButton>
              </Excercise>
            ))}
          </Excercises>
          <HabitHeader>Source & more info</HabitHeader>
          {_.split(habitDetails.sources, '\n').map((source, index) => (
            <HabitSource key={index}>
              <OpenSource name="log-out" type="Entypo" />
              {this._getSourceTitle(source)}
            </HabitSource>
          ))}
          <StartHabit>
            <StartHabitText>Start habit</StartHabitText>
            <Text />
          </StartHabit>
        </Content>
      </ScrollView>
    );
  }
}
